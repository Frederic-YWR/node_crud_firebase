import {HotelModel} from '../models/hotel.model';
import {certificat} from '../cert/certificat_json';
import admin from 'firebase-admin';
import DocumentReference = FirebaseFirestore.DocumentReference;
import DocumentData = FirebaseFirestore.DocumentData;
import DocumentSnapshot = FirebaseFirestore.DocumentSnapshot;
import QuerySnapshot = FirebaseFirestore.QuerySnapshot;
import CollectionReference = FirebaseFirestore.CollectionReference;
import Transaction = FirebaseFirestore.Transaction;
import WriteResult = FirebaseFirestore.WriteResult;



admin.initializeApp({
  credential: admin.credential.cert(certificat),
  databaseURL: "https://hotels-1c82a.firebaseio.com"
});

const db = admin.firestore();

const refHotels = db.collection('hotels');


export async function getHostel() : Promise<QuerySnapshot> {

     const hostelsQuerySnap: QuerySnapshot = await refHotels.get(); 
     
     return hostelsQuerySnap
}
export async function getAllHostels(): Promise<HotelModel[]> {
    const hostelsQuerySnap: QuerySnapshot = await refHotels.get();
    const hotels: HotelModel[] = [];
    hostelsQuerySnap.forEach(hotelSnap => hotels.push(hotelSnap.data() as HotelModel));
    return hotels;
}
export async function postNewHostel(newHotel:HotelModel):Promise<WriteResult>  {

    
    const hotels :QuerySnapshot = await refHotels.get();
    const newsize:number = hotels.size + 1;
    const writeHotel:DocumentReference = await refHotels.doc('hostel'+newsize);
    const dataAdded:WriteResult   = await writeHotel.set(newHotel);
      
    return dataAdded
}
export async function putHostel(hostel:string,newData:HotelModel):Promise<WriteResult>  {

    
    const writeHotel:DocumentReference = await refHotels.doc(hostel);
    const dataAdded:WriteResult   = await writeHotel.set(newData);
      
    return dataAdded
}
export async function patchHostel(hostel:string,newData:object):Promise<WriteResult>  {

    
    const writeHotel:DocumentReference = await refHotels.doc(hostel);
    const dataUpdated:WriteResult  = await writeHotel.update(newData);
      
    return dataUpdated
}
export async function deleteHostel(hostel:string):Promise<WriteResult>  {

    
    const writeHotel:DocumentReference = await refHotels.doc(hostel);
    const dataDeleted:WriteResult  = await writeHotel.delete();
      
    return dataDeleted
}