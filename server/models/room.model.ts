
export interface RoomModel {
    id: number;
    size: number;
    roomName: string;
    roomState?: RoomState | undefined;
}

export enum RoomState {
    isAvailable = 'est libre',
    isCleaning = 'en nettoyage',
    isOccupied = 'occupée',
    isClosed = 'en travaux',
    sjksjksjk = 'tout moche',
}