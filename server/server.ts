import express from 'express';
import cors from 'cors';
import bodyParser from 'body-parser';
import {getHostel,postNewHostel,putHostel,deleteHostel,patchHostel} from './services/hostels.service';

const app = express();
app.use(cors());
app.use(bodyParser.json());
app.use(cors());

app.get('/api', async (req, res) => {
    return res.send('coucou promo 4');
});

app.get('/api/hotels', async (req,res)=> {
    try {
        let arr = await getHostel();
        return res.send(arr)
    }catch(e){
        return res.status(500).send({error:'erreur serveur :'+ e.message})
}
});

app.post('/api/hotels', async (req,res)=> {
    try {
        let dataPosted = req.body;
        let api_post_call = await postNewHostel(dataPosted );
        return res.send(api_post_call)
    }catch(e){
        return res.status(500).send({error:'erreur serveur :'+ e.message})
}
});
app.delete('/api/hotels/:id', async (req,res)=> {
    try {
        
        let hotel = 'hostel'+req.params.id;
        let api_post_call = await deleteHostel(hotel);
        return res.send(api_post_call)
    }catch(e){
        return res.status(500).send({error:'erreur serveur :'+ e.message})
}
});
app.put('/api/hotels/:id', async (req,res)=> {
    try {
        let dataPutted = req.body;
        let hotel = 'hostel'+req.params.id;
        let api_post_call = await putHostel(hotel,dataPutted);
        return res.send(api_post_call)
    }catch(e){
        return res.status(500).send({error:'erreur serveur :'+ e.message})
}
});
app.patch('/api/hotels/:id', async (req,res)=> {
    try {
    
        let dataPatched  = req.body;
        let hotel = 'hostel'+req.params.id;
        let api_post_call = await patchHostel(hotel,dataPatched);
        return res.send(api_post_call)
    }catch(e){
        return res.status(500).send({error:'erreur serveur :'+ e.message})
}
});


app.listen(3015, function () {
    console.log('API listening on http://localhost:3015/api/ !');
});


