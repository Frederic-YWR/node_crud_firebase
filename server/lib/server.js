"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
exports.__esModule = true;
var express_1 = require("express");
var cors_1 = require("cors");
var body_parser_1 = require("body-parser");
var hostels_service_1 = require("./services/hostels.service");
var app = express_1();
app.use(cors_1());
app.use(body_parser_1.json());
app.use(cors_1());
app.get('/api', function (req, res) { return __awaiter(void 0, void 0, void 0, function () {
    return __generator(this, function (_a) {
        return [2 /*return*/, res.send('coucou promo 4')];
    });
}); });
app.get('/api/hotels', function (req, res) { return __awaiter(void 0, void 0, void 0, function () {
    var arr, e_1;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                _a.trys.push([0, 2, , 3]);
                return [4 /*yield*/, hostels_service_1.getHostel()];
            case 1:
                arr = _a.sent();
                return [2 /*return*/, res.send(arr)];
            case 2:
                e_1 = _a.sent();
                return [2 /*return*/, res.status(500).send({ error: 'erreur serveur :' + e_1.message })];
            case 3: return [2 /*return*/];
        }
    });
}); });
app.post('/api/hotels', function (req, res) { return __awaiter(void 0, void 0, void 0, function () {
    var hotel, api_post_call, e_2;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                _a.trys.push([0, 2, , 3]);
                hotel = {
                    name: 'hotel mixte',
                    roomNumbers: 7,
                    pool: true,
                    rooms: [3, 5, 8, 9, 6]
                };
                return [4 /*yield*/, hostels_service_1.postNewHostel(hotel)];
            case 1:
                api_post_call = _a.sent();
                return [2 /*return*/, res.send(api_post_call)];
            case 2:
                e_2 = _a.sent();
                return [2 /*return*/, res.status(500).send({ error: 'erreur serveur :' + e_2.message })];
            case 3: return [2 /*return*/];
        }
    });
}); });
app["delete"]('/api/hotels/:id', function (req, res) { return __awaiter(void 0, void 0, void 0, function () {
    var hotel, api_post_call, e_3;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                _a.trys.push([0, 2, , 3]);
                hotel = 'hostel' + req.params.id;
                return [4 /*yield*/, hostels_service_1.deleteHostel(hotel)];
            case 1:
                api_post_call = _a.sent();
                return [2 /*return*/, res.send(api_post_call)];
            case 2:
                e_3 = _a.sent();
                return [2 /*return*/, res.status(500).send({ error: 'erreur serveur :' + e_3.message })];
            case 3: return [2 /*return*/];
        }
    });
}); });
app.put('/api/hotels/:id', function (req, res) { return __awaiter(void 0, void 0, void 0, function () {
    var newdata, hotel, api_post_call, e_4;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                _a.trys.push([0, 2, , 3]);
                newdata = req.body;
                hotel = 'hostel' + req.params.id;
                return [4 /*yield*/, hostels_service_1.putHostel(hotel, newdata)];
            case 1:
                api_post_call = _a.sent();
                return [2 /*return*/, res.send(api_post_call)];
            case 2:
                e_4 = _a.sent();
                return [2 /*return*/, res.status(500).send({ error: 'erreur serveur :' + e_4.message })];
            case 3: return [2 /*return*/];
        }
    });
}); });
app.patch('/api/hotels/:id', function (req, res) { return __awaiter(void 0, void 0, void 0, function () {
    var newData, hotel, api_post_call, e_5;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                _a.trys.push([0, 2, , 3]);
                newData = { rooms: [2, 5, 8, 4, 10] };
                hotel = 'hostel' + req.params.id;
                return [4 /*yield*/, hostels_service_1.patchHostel(hotel, newData)];
            case 1:
                api_post_call = _a.sent();
                return [2 /*return*/, res.send(api_post_call)];
            case 2:
                e_5 = _a.sent();
                return [2 /*return*/, res.status(500).send({ error: 'erreur serveur :' + e_5.message })];
            case 3: return [2 /*return*/];
        }
    });
}); });
app.listen(3015, function () {
    console.log('API listening on http://localhost:3015/api/ !');
});
