"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
exports.__esModule = true;
exports.deleteHostel = exports.patchHostel = exports.putHostel = exports.postNewHostel = exports.getAllHostels = exports.getHostel = void 0;
var certificat_json_1 = require("../cert/certificat_json");
var firebase_admin_1 = require("firebase-admin");
firebase_admin_1["default"].initializeApp({
    credential: firebase_admin_1["default"].credential.cert(certificat_json_1.certificat),
    databaseURL: "https://hotels-1c82a.firebaseio.com"
});
var db = firebase_admin_1["default"].firestore();
var refHotels = db.collection('hotels');
function getHostel() {
    return __awaiter(this, void 0, void 0, function () {
        var hostelsQuerySnap;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4 /*yield*/, refHotels.get()];
                case 1:
                    hostelsQuerySnap = _a.sent();
                    return [2 /*return*/, hostelsQuerySnap];
            }
        });
    });
}
exports.getHostel = getHostel;
function getAllHostels() {
    return __awaiter(this, void 0, void 0, function () {
        var hostelsQuerySnap, hotels;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4 /*yield*/, refHotels.get()];
                case 1:
                    hostelsQuerySnap = _a.sent();
                    hotels = [];
                    hostelsQuerySnap.forEach(function (hotelSnap) { return hotels.push(hotelSnap.data()); });
                    return [2 /*return*/, hotels];
            }
        });
    });
}
exports.getAllHostels = getAllHostels;
function postNewHostel(newHotel) {
    return __awaiter(this, void 0, void 0, function () {
        var hotels, newsize, writeHotel, dataAdded;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4 /*yield*/, refHotels.get()];
                case 1:
                    hotels = _a.sent();
                    newsize = hotels.size + 1;
                    return [4 /*yield*/, refHotels.doc('hostel' + newsize)];
                case 2:
                    writeHotel = _a.sent();
                    return [4 /*yield*/, writeHotel.set(newHotel)];
                case 3:
                    dataAdded = _a.sent();
                    return [2 /*return*/, dataAdded];
            }
        });
    });
}
exports.postNewHostel = postNewHostel;
function putHostel(hostel, newData) {
    return __awaiter(this, void 0, void 0, function () {
        var writeHotel, dataAdded;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4 /*yield*/, refHotels.doc(hostel)];
                case 1:
                    writeHotel = _a.sent();
                    return [4 /*yield*/, writeHotel.set(newData)];
                case 2:
                    dataAdded = _a.sent();
                    return [2 /*return*/, dataAdded];
            }
        });
    });
}
exports.putHostel = putHostel;
function patchHostel(hostel, newData) {
    return __awaiter(this, void 0, void 0, function () {
        var writeHotel, dataUpdated;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4 /*yield*/, refHotels.doc(hostel)];
                case 1:
                    writeHotel = _a.sent();
                    return [4 /*yield*/, writeHotel.update(newData)];
                case 2:
                    dataUpdated = _a.sent();
                    return [2 /*return*/, dataUpdated];
            }
        });
    });
}
exports.patchHostel = patchHostel;
function deleteHostel(hostel) {
    return __awaiter(this, void 0, void 0, function () {
        var writeHotel, dataDeleted;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4 /*yield*/, refHotels.doc(hostel)];
                case 1:
                    writeHotel = _a.sent();
                    return [4 /*yield*/, writeHotel["delete"]()];
                case 2:
                    dataDeleted = _a.sent();
                    return [2 /*return*/, dataDeleted];
            }
        });
    });
}
exports.deleteHostel = deleteHostel;
